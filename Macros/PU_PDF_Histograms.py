#from ROOT import gSystem
# Example Execution
# python PU_PDF_Histograms.py "/hep300/data/cranelli/LAr/mc15_14TeV/PU/AREUS_NTUPLE/Nico_Ntuples/user.nmadysa.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.merge.AREUS.e1133_s2943_t610_StreamNTUP_GEN" "pu_pdf_histograms.root"

from ROOT import TFile
from ROOT import TTree
from ROOT import TH1F, TH2F
from ROOT import TProfile

from array import *

import sys

TREE_NAME = "gend3pd"

ETA_EDGES= [ 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.7, 2.9, 3.1, 3.3 ]
LAYER_EDGES= [-0.5, 0.5, 1.5, 2.5, 3.5]


#outfile_name = "minbias_lowpt_ptcuts_histograms.root"

def PDFHistograms(infile_loc, outfile_loc):

    infile = TFile(infile_loc, "READ")
    outfile =TFile(outfile_loc, "RECREATE")
    analysis_tree = infile.Get(TREE_NAME)

    num_entries = analysis_tree.GetEntries()
    print "Number of entries:", num_entries

    #Histograms
    abseta_hist = TH1F("h_abseta", "h_abseta", len(ETA_EDGES)-1, array('d', ETA_EDGES) )
    layer_hist = TH1F("h_layer", "h_layer", len(LAYER_EDGES)-1, array('d', LAYER_EDGES) )
    abseta_layer_hist = TH2F("h_abseta_layer", "h_abseta_layer", len(ETA_EDGES)-1, array('d', ETA_EDGES), len(LAYER_EDGES)-1, array('d', LAYER_EDGES))
    sumE_hist = TH2F("h_sumE", "h_sumE", len(ETA_EDGES)-1, array('d', ETA_EDGES), len(LAYER_EDGES)-1, array('d', LAYER_EDGES))
    q_hist = TH2F("h_q", "h_q", len(ETA_EDGES)-1, array('d', ETA_EDGES), len(LAYER_EDGES)-1, array('d', LAYER_EDGES))

    # Loop Over Events
    for entry in xrange(num_entries):
        analysis_tree.GetEntry(entry)
        hechit_n = analysis_tree.hithec_n 
       
         #For Each Event, Clear the Enegy Sum
        sumE_hist.Reset()
        
        # Loop Over HITS in an event
        for hechit_index in xrange(hechit_n):
            abseta = abs(analysis_tree.hithec_eta[hechit_index])
            abseta_hist.Fill(abseta)
            layer = analysis_tree.hithec_ID[hechit_index]/512
            layer_hist.Fill(layer)
            abseta_layer_hist.Fill(abseta, layer)

            energy = analysis_tree.hithec_E[hechit_index]

            sumE(sumE_hist, abseta, layer, energy)
            
        CountZeros(q_hist, sumE_hist)
        
            
    outfile.cd()
    abseta_hist.Write()
    layer_hist.Write()
    abseta_layer_hist.Write()
    sumE_hist.Write()
    q_hist.Write()

def sumE(sumE_hist, abseta, layer, energy):
    bin = sumE_hist.FindBin(abseta, layer)
    energy_sum = sumE_hist.GetBinContent(bin)
    #if energy_sum != 0: print "double hit"
    energy_sum += energy
    sumE_hist.SetBinContent(bin, energy_sum)
    
def CountZeros(q_hist, sumE_hist):
    #for eta_index in range(1, sumE_hist.GetNbinsX()):
    #    for 
    for bin in range(1, sumE_hist.GetNbinsX() * sumE_hist.GetNbinsY()):
        zero_count = q_hist.GetBinContent(bin)
        if sumE_hist.GetBinContent(bin) == 0:
            zero_count += 1
            q_hist.SetBinContent(bin, zero_count)


if __name__=="__main__":
    infile_loc = sys.argv[1]
    outfile_loc = sys.argv[2]
    PDFHistograms(infile_loc, outfile_loc)
