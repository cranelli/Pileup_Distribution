#from ROOT import gSystem
# Example Execution
# python MultiplicityHistograms "/hep300/data/cranelli/LAr/mc15_14TeV/AREUS_NTUPLE/minbias_high_areusntuple_610.root"  "pu_pdf_histograms.root"

from ROOT import TFile
from ROOT import TTree
from ROOT import TH1F, TH2F
from ROOT import TProfile

import sys

infile_loc = ""
outfile_loc = ""
treename = "gend3pd"

#outfile_name = "minbias_lowpt_ptcuts_histograms.root"

if __name__=="__main__":
    infile_loc = sys.argv[1]
    outfile_loc = sys.argv[2]


infile = TFile(infile_loc, "READ")
outfile =TFile(outfile_loc, "RECREATE")
analysis_tree = infile.Get(treename)



#Histograms
#h_mc_n = TH1F("mc_n", "mc_n", 100, 0, 6000)
#h_mc_finalstate_n = TH1F("mc_finalstate_n", "mc_finalstate_n", 100, 0, 6000)
#h_hit_n = TH1F("hit_n", "hit_n", 100, 0, 6000)
#h_mc_finalstate_n = TH1F("mc_finalstate_n", "mc_finalstate_n", 100, 0, 6000)


NUM_ETA_BINS = 60
NUM_PT_BINS = 40
h_temp_eta = TH1F("eta", "eta", NUM_ETA_BINS, -2.5, 2.5)
prf_temp_eta_pt = TProfile("prf_eta_pt", "Temporary Profile of Pt vs Eta", NUM_ETA_BINS, -2.5, 2.5) 

#h2_mc_finalstate_n_eta = TH2F("finaln_eta", "Number of particles vs eta", NUM_ETA_BINS, -5, 5, 100, 0, 5000)
prf_n_eta = TProfile("prf_n_eta", "Profile of Pion N vs #eta", NUM_ETA_BINS, -2.5, 2.5)
prf_avgpt_eta = TProfile("prf_avgpt_eta", "Profile of <Pt> vs #eta", NUM_ETA_BINS, -5, 5)
prf_sumpt_eta = TProfile("prf_sumpt_eta", "Profile of #sum Pt vs #eta", NUM_ETA_BINS, -5, 5)
#h_hittemp_eta = TH1F("hit_eta", "hit_eta", NUM_ETA_BINS, -5, 5)
#prf_hitn_eta = TProfile("prf_hitn_eta", "Profile of N Hits vs #eta", NUM_ETA_BINS, -5, 5)


num_entries = analysis_tree.GetEntries()
print "Number of entries:", num_entries
for entry in xrange(num_entries):

    analysis_tree.GetEntry(entry)
    
    #mc_n = analysis_tree.mc_n
    mc_status = analysis_tree.mc_status
    mc_type = analysis_tree.mc_type
    mc_eta = analysis_tree.mc_eta
    #mc_pt = analysis_tree.mc_pt

    #hit_n = analysis_tree.hitemb_n
    #hitemb_eta = analysis_tree.hitemb_eta
    
    # loop over generator level particles
    for mc_index in xrange(mc_n):
        
        #Final State Particles
        if mc_status[mc_index] == 1:
            if abs(mc_type[mc_index]) == 211: #Pions
                if mc_pt[mc_index] > 500:
                    mc_finalstate_n += 1
                    h_temp_eta.Fill(mc_eta[mc_index])
                    prf_temp_eta_pt.Fill(mc_eta[mc_index], mc_pt[mc_index])

    for hit_index in xrange(hit_n):
        h_hittemp_eta.Fill(hitemb_eta[hit_index])
            
    #print prf_temp_eta_pt.GetBinEntries(1)
    # Fill Histograms
    #h_mc_n.Fill(mc_n)
    #h_mc_finalstate_n.Fill(mc_finalstate_n)


    h_hit_n.Fill(hit_n)


    for bin in xrange(h_temp_eta.GetNbinsX()):
        #if h_temp_eta.GetBinContent(bin) != 0:  #require non-zero entry
            #h2_mc_finalstate_n_eta.Fill(h_temp_eta.GetBinCenter(bin), h_temp_eta.GetBinContent(bin))
            prf_n_eta.Fill(h_temp_eta.GetBinCenter(bin), h_temp_eta.GetBinContent(bin))
            prf_avgpt_eta.Fill(h_temp_eta.GetBinCenter(bin), prf_temp_eta_pt.GetBinContent(bin))
            prf_sumpt_eta.Fill(h_temp_eta.GetBinCenter(bin), prf_temp_eta_pt.GetBinContent(bin) * prf_temp_eta_pt.GetBinEntries(bin))
    

    for bin in xrange(h_hittemp_eta.GetNbinsX()):
        prf_hitn_eta.Fill(h_hittemp_eta.GetBinCenter(bin), h_hittemp_eta.GetBinContent(bin))
        


    h_temp_eta.Reset()
    h_hittemp_eta.Reset()
    prf_temp_eta_pt.Reset()

# Output Histograms    
outfile.cd()
h_mc_n.Write()
h_mc_finalstate_n.Write()
h_mc_finalstate_n.GetXaxis().SetTitle("N")

prf_n_eta.GetXaxis().SetTitle("#eta")
prf_n_eta.GetYaxis().SetTitle("<N>")
prf_n_eta.Write()

prf_hitn_eta.GetXaxis().SetTitle("#eta")
prf_hitn_eta.GetYaxis().SetTitle("<N Hits>")
prf_hitn_eta.Write()


prf_avgpt_eta.GetXaxis().SetTitle("#eta")
prf_avgpt_eta.GetYaxis().SetTitle("<<Pt>> MeV")
prf_avgpt_eta.Write()

prf_sumpt_eta.GetXaxis().SetTitle("#eta")
prf_sumpt_eta.GetYaxis().SetTitle("<#sum Pt> MeV")
prf_sumpt_eta.Write()
#h2_mc_finalstate_n_eta.Write()
