#ifndef __HISTOGRAMSTORE_H__
#define __HISTOGRAMSTORE_H__

/*                                                                                                
 * Author Chris Anelli                                                                            
 * 4.24.2017                                                                                     
 */

#include <iostream>
#include <map>
#include "TH1.h"

using namespace std;

class HistogramStore{
 public:
  void Fill(string key, float val, double weight=1, int n_bins=100, float xmin=0, float xmax=100.0, string xaxis_title="", string yaxis_title="");
  //void Fill3D_Variable(string key, float val_x, float val_y, float val_z, double weight, int nbins_x, float * xbins, int nbins_y, float * ybins, int nbins_z, float *zbins, string xaxis_title="", string yaxis_title="", string zaxis_title="");  
  void Write();

 private:
  map<string, TH1*> histograms_;

};

#endif
