//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue May  2 22:55:29 2017 by ROOT version 6.04/14
// from TTree gend3pd/gend3pd
// found on file: /hep300/data/cranelli/LAr/mc15_14TeV/PU/AREUS_NTUPLE/minbias_low_areusntuple_610.root
//////////////////////////////////////////////////////////

#ifndef pileup_pdf_h
#define pileup_pdf_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "histogram_store.h"

// Header file for the classes stored in the TTree if any.
#include "vector"


using namespace std;

class pileup_pdf {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   //My Stuff
   HistogramStore histogram_store;

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           hitemb_n;
   vector<float>   *hitemb_eta;
   vector<float>   *hitemb_phi;
   vector<float>   *hitemb_E;
   vector<float>   *hitemb_Time;
   vector<unsigned int> *hitemb_ID;
   vector<int>     *hitemb_ieta;
   vector<int>     *hitemb_iphi;
   Int_t           hitemec_n;
   vector<float>   *hitemec_eta;
   vector<float>   *hitemec_phi;
   vector<float>   *hitemec_E;
   vector<float>   *hitemec_Time;
   vector<unsigned int> *hitemec_ID;
   vector<int>     *hitemec_ieta;
   vector<int>     *hitemec_iphi;
   Int_t           hithec_n;
   vector<float>   *hithec_eta;
   vector<float>   *hithec_phi;
   vector<float>   *hithec_E;
   vector<float>   *hithec_Time;
   vector<unsigned int> *hithec_ID;
   vector<int>     *hithec_ieta;
   vector<int>     *hithec_iphi;
   Int_t           hittile_n;
   vector<float>   *hittile_energy;
   vector<float>   *hittile_time;
   vector<int>     *hittile_detector;
   vector<int>     *hittile_side;
   vector<int>     *hittile_sample;
   vector<int>     *hittile_pmt;
   vector<int>     *hittile_eta;
   vector<int>     *hittile_phi;
   Int_t           mc_n;
   vector<float>   *mc_pt;
   vector<float>   *mc_m;
   vector<float>   *mc_eta;
   vector<float>   *mc_phi;
   vector<int>     *mc_type;
   vector<int>     *mc_status;
   vector<int>     *mc_barcode;
   vector<int>     *mc_mothertype;
   vector<int>     *mc_motherbarcode;
   vector<int>     *mc_mother_n;
   vector<vector<int> > *mc_mother_index;
   vector<int>     *mc_child_n;
   vector<vector<int> > *mc_child_index;

   // List of branches
   TBranch        *b_hitemb_n;   //!
   TBranch        *b_hitemb_eta;   //!
   TBranch        *b_hitemb_phi;   //!
   TBranch        *b_hitemb_E;   //!
   TBranch        *b_hitemb_Time;   //!
   TBranch        *b_hitemb_ID;   //!
   TBranch        *b_hitemb_ieta;   //!
   TBranch        *b_hitemb_iphi;   //!
   TBranch        *b_hitemec_n;   //!
   TBranch        *b_hitemec_eta;   //!
   TBranch        *b_hitemec_phi;   //!
   TBranch        *b_hitemec_E;   //!
   TBranch        *b_hitemec_Time;   //!
   TBranch        *b_hitemec_ID;   //!
   TBranch        *b_hitemec_ieta;   //!
   TBranch        *b_hitemec_iphi;   //!
   TBranch        *b_hithec_n;   //!
   TBranch        *b_hithec_eta;   //!
   TBranch        *b_hithec_phi;   //!
   TBranch        *b_hithec_E;   //!
   TBranch        *b_hithec_Time;   //!
   TBranch        *b_hithec_ID;   //!
   TBranch        *b_hithec_ieta;   //!
   TBranch        *b_hithec_iphi;   //!
   TBranch        *b_hittile_n;   //!
   TBranch        *b_hittile_energy;   //!
   TBranch        *b_hittile_time;   //!
   TBranch        *b_hittile_detector;   //!
   TBranch        *b_hittile_side;   //!
   TBranch        *b_hittile_sample;   //!
   TBranch        *b_hittile_pmt;   //!
   TBranch        *b_hittile_eta;   //!
   TBranch        *b_hittile_phi;   //!
   TBranch        *b_mc_n;   //!
   TBranch        *b_mc_pt;   //!
   TBranch        *b_mc_m;   //!
   TBranch        *b_mc_eta;   //!
   TBranch        *b_mc_phi;   //!
   TBranch        *b_mc_type;   //!
   TBranch        *b_mc_status;   //!
   TBranch        *b_mc_barcode;   //!
   TBranch        *b_mc_mothertype;   //!
   TBranch        *b_mc_motherbarcode;   //!
   TBranch        *b_mc_mother_n;   //!
   TBranch        *b_mc_mother_index;   //!
   TBranch        *b_mc_child_n;   //!
   TBranch        *b_mc_child_index;   //!

   pileup_pdf(TTree *tree=0);
   virtual ~pileup_pdf();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

 private:
   void ResetCells( vector<float> & hec_cells);
   void CountZeroEnergyCells( vector<float> & hec_cells, vector<int> & zero_count);
   void FillEnergyHistograms(vector<float> & hec_cells);
   void LoopPhi_EnergyFill(int layer, float eta, int num_phis, vector<float> & hec_cells);
   int GetCellIndex(int id, float eta, float phi);
   int GetLayerIndex(unsigned int hit_ID) { return hit_ID/512;};
   int GetEtaIndex(float eta);
   int GetPhiIndex(float phi, float eta);
   void CalcQ(vector<int> & zero_counts, int nentries);
   float LoopPhi_Avgq(vector<int> & zero_counts, int layer, float eta, int num_phi_cells);
   void TestMapping();

};

#endif

#ifdef pileup_pdf_cxx
pileup_pdf::pileup_pdf(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/hep300/data/cranelli/LAr/mc15_14TeV/PU/AREUS_NTUPLE/minbias_low_areusntuple_610.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/hep300/data/cranelli/LAr/mc15_14TeV/PU/AREUS_NTUPLE/minbias_low_areusntuple_610.root");
      }
      f->GetObject("gend3pd",tree);

   }
   Init(tree);
}

pileup_pdf::~pileup_pdf()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t pileup_pdf::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t pileup_pdf::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void pileup_pdf::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   hitemb_eta = 0;
   hitemb_phi = 0;
   hitemb_E = 0;
   hitemb_Time = 0;
   hitemb_ID = 0;
   hitemb_ieta = 0;
   hitemb_iphi = 0;
   hitemec_eta = 0;
   hitemec_phi = 0;
   hitemec_E = 0;
   hitemec_Time = 0;
   hitemec_ID = 0;
   hitemec_ieta = 0;
   hitemec_iphi = 0;
   hithec_eta = 0;
   hithec_phi = 0;
   hithec_E = 0;
   hithec_Time = 0;
   hithec_ID = 0;
   hithec_ieta = 0;
   hithec_iphi = 0;
   hittile_energy = 0;
   hittile_time = 0;
   hittile_detector = 0;
   hittile_side = 0;
   hittile_sample = 0;
   hittile_pmt = 0;
   hittile_eta = 0;
   hittile_phi = 0;
   mc_pt = 0;
   mc_m = 0;
   mc_eta = 0;
   mc_phi = 0;
   mc_type = 0;
   mc_status = 0;
   mc_barcode = 0;
   mc_mothertype = 0;
   mc_motherbarcode = 0;
   mc_mother_n = 0;
   mc_mother_index = 0;
   mc_child_n = 0;
   mc_child_index = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("hitemb_n", &hitemb_n, &b_hitemb_n);
   fChain->SetBranchAddress("hitemb_eta", &hitemb_eta, &b_hitemb_eta);
   fChain->SetBranchAddress("hitemb_phi", &hitemb_phi, &b_hitemb_phi);
   fChain->SetBranchAddress("hitemb_E", &hitemb_E, &b_hitemb_E);
   fChain->SetBranchAddress("hitemb_Time", &hitemb_Time, &b_hitemb_Time);
   fChain->SetBranchAddress("hitemb_ID", &hitemb_ID, &b_hitemb_ID);
   fChain->SetBranchAddress("hitemb_ieta", &hitemb_ieta, &b_hitemb_ieta);
   fChain->SetBranchAddress("hitemb_iphi", &hitemb_iphi, &b_hitemb_iphi);
   fChain->SetBranchAddress("hitemec_n", &hitemec_n, &b_hitemec_n);
   fChain->SetBranchAddress("hitemec_eta", &hitemec_eta, &b_hitemec_eta);
   fChain->SetBranchAddress("hitemec_phi", &hitemec_phi, &b_hitemec_phi);
   fChain->SetBranchAddress("hitemec_E", &hitemec_E, &b_hitemec_E);
   fChain->SetBranchAddress("hitemec_Time", &hitemec_Time, &b_hitemec_Time);
   fChain->SetBranchAddress("hitemec_ID", &hitemec_ID, &b_hitemec_ID);
   fChain->SetBranchAddress("hitemec_ieta", &hitemec_ieta, &b_hitemec_ieta);
   fChain->SetBranchAddress("hitemec_iphi", &hitemec_iphi, &b_hitemec_iphi);
   fChain->SetBranchAddress("hithec_n", &hithec_n, &b_hithec_n);
   fChain->SetBranchAddress("hithec_eta", &hithec_eta, &b_hithec_eta);
   fChain->SetBranchAddress("hithec_phi", &hithec_phi, &b_hithec_phi);
   fChain->SetBranchAddress("hithec_E", &hithec_E, &b_hithec_E);
   fChain->SetBranchAddress("hithec_Time", &hithec_Time, &b_hithec_Time);
   fChain->SetBranchAddress("hithec_ID", &hithec_ID, &b_hithec_ID);
   fChain->SetBranchAddress("hithec_ieta", &hithec_ieta, &b_hithec_ieta);
   fChain->SetBranchAddress("hithec_iphi", &hithec_iphi, &b_hithec_iphi);
   fChain->SetBranchAddress("hittile_n", &hittile_n, &b_hittile_n);
   fChain->SetBranchAddress("hittile_energy", &hittile_energy, &b_hittile_energy);
   fChain->SetBranchAddress("hittile_time", &hittile_time, &b_hittile_time);
   fChain->SetBranchAddress("hittile_detector", &hittile_detector, &b_hittile_detector);
   fChain->SetBranchAddress("hittile_side", &hittile_side, &b_hittile_side);
   fChain->SetBranchAddress("hittile_sample", &hittile_sample, &b_hittile_sample);
   fChain->SetBranchAddress("hittile_pmt", &hittile_pmt, &b_hittile_pmt);
   fChain->SetBranchAddress("hittile_eta", &hittile_eta, &b_hittile_eta);
   fChain->SetBranchAddress("hittile_phi", &hittile_phi, &b_hittile_phi);
   fChain->SetBranchAddress("mc_n", &mc_n, &b_mc_n);
   fChain->SetBranchAddress("mc_pt", &mc_pt, &b_mc_pt);
   fChain->SetBranchAddress("mc_m", &mc_m, &b_mc_m);
   fChain->SetBranchAddress("mc_eta", &mc_eta, &b_mc_eta);
   fChain->SetBranchAddress("mc_phi", &mc_phi, &b_mc_phi);
   fChain->SetBranchAddress("mc_type", &mc_type, &b_mc_type);
   fChain->SetBranchAddress("mc_status", &mc_status, &b_mc_status);
   fChain->SetBranchAddress("mc_barcode", &mc_barcode, &b_mc_barcode);
   fChain->SetBranchAddress("mc_mothertype", &mc_mothertype, &b_mc_mothertype);
   fChain->SetBranchAddress("mc_motherbarcode", &mc_motherbarcode, &b_mc_motherbarcode);
   fChain->SetBranchAddress("mc_mother_n", &mc_mother_n, &b_mc_mother_n);
   fChain->SetBranchAddress("mc_mother_index", &mc_mother_index, &b_mc_mother_index);
   fChain->SetBranchAddress("mc_child_n", &mc_child_n, &b_mc_child_n);
   fChain->SetBranchAddress("mc_child_index", &mc_child_index, &b_mc_child_index);
   Notify();
}

Bool_t pileup_pdf::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void pileup_pdf::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t pileup_pdf::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef pileup_pdf_cxx
