#!/usr/bin/env python

# Python script 
# the Histograms from two root files.
# Example:
# python MakeAscii.py  sum_minbias_low_high.root

import sys

from ROOT import TFile
from ROOT import TH1F
from ROOT import TH2F

#doCalcC=True

def hist2txt(file1Loc):

    #Iterate over all the Histograms in the first File
    file1 = TFile(file1Loc, 'READ')
    
    loopList(file1)

def loopList(dir1):
    list = dir1.GetListOfKeys()
    for key in list:
        # Loop Over Sub Directories
        if key.GetClassName() == "TDirectoryFile":
            print "Directory" , key.GetName()
            new_dir1 = dir1.Get(key.GetName())
            loopList(new_dir1)
            continue
        
        # Skip Trees
        if key.GetClassName() == "TTree" : continue
    
        # Combine and Write Histograms
        histName = key.GetName()

        if "MeV" in histName : continue
        print histName
        hist1 = dir1.Get(histName)
        f = open(histName+".dat", 'a')
        for bin in range(1,hist1.GetNbinsX()+1):
            #print hist1.GetBinCenter(bin) , "," , hist1.GetBinContent(bin)
            f.write( str(hist1.GetBinCenter(bin)) +  " , " + str(hist1.GetBinContent(bin)) + '\n' )
        f.close()

           
if __name__=="__main__":
    hist2txt(sys.argv[1])
    
