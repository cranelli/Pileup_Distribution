#!/usr/bin/env python 
#Example
#./SamplePU.py --q 0.967 --histname Energy_0_eta1.55


import sys
import argparse as ap
import random
import math


#import statistics
import numpy as np
#from scipy.stats import poisson

from ROOT import TFile
from ROOT import TH1F

root_file = TFile("sum_minbias_low_high.root", "READ")
out_file = TFile("sampling_output.root", "RECREATE")

# Calculates the standard deviation for the single minbias distribtuion sampled 
# different number of times. q-value and name of the histogram with 
# minbias energy distribution are supplied by the user.

def PrintOut(q, histname):
	hist = root_file.Get(histname) 
        eta = histname.split('_')[2].strip("eta")
	

        sample_hist_1 = SampleN(q, hist, 1)
	#print "Mean for n=1 (analytic) is ", CalcMean1(q, hist), "MeV"

        #print "Standard Deviation for n=1 (sampled) is: ", sample_hist_1.GetStdDev(), "MeV"
	#print "Standard Deviation for n=1 (analytic) is ", CalcStdDev1(q, hist), "MeV"
	#print "sqrt(200) time sigma1 (analytic) is ", CalcStdDev1(q, hist) * 14.14,  "MeV"

        sample_hist_200 = SampleN(q, hist, 200)
        #print "Standard Deviation for n=200 is: ", sample_hist_200.GetStdDev(), "MeV"
	
	sample_hist_mu_200 = SampleMu(q, hist, 200)
	#print "Standard Deviation for mu=200 is: ", sample_hist_mu_200.GetStdDev(), "MeV"
	#print "Expected standard deviation for mu =200: ", pow( 200 * (pow(CalcStdDev1(q, hist), 2) + pow(CalcMean1(q, hist), 2) ) , 0.5 ),  "MeV"
	#print "RMS for n=200 is: ", sample_hist_200.GetRMS(), "MeV"
	#print eta , " & ", q, " & ",  "{:.3f}".format( CalcStdDev1(q, hist) ), " & ", "{:.3f}".format( CalcStdDev1(q, hist) * 14.14 ),  " & ", "{:.3f}".format(sample_hist_200.GetStdDev() ), " & " , "{:.3f}".format( (CalcStdDev1(q, hist) * 14.14) / sample_hist_200.GetStdDev() ),  r'\\'
	print eta , " & ", q, " & ", "{:.3f}".format(sample_hist_200.GetStdDev() ), " &" , "{:.3f}".format(sample_hist_mu_200.GetStdDev() ), " & ", "{:.3f}".format(sample_hist_mu_200.GetStdDev() / sample_hist_200.GetStdDev() ),  " & ",  "{:.3f}".format(pow( 200 * (pow(CalcStdDev1(q, hist), 2) + pow(CalcMean1(q, hist), 2)) , 0.5) ), " &" , "{:.3f}".format(pow( 200 * (pow(CalcStdDev1(q, hist), 2) + pow(CalcMean1(q, hist), 2)) , 0.5) / sample_hist_mu_200.GetStdDev() , r'\\'  )

	out_file.Write();

# Returns a histogram for a single minbias distribution sampled N times.
def SampleN(q, hist, N):
	
    sum_hist = TH1F("sample" + str(N), "sample"+str(N), 1500, 0, math.ceil( (1-q)*N) * 1500) #variable histogram sizes
    sum_hist.Sumw2()
    random.seed(1)

    for i in range(0, 100000):
	    s = Sum(q, hist, N)
	    sum_hist.Fill(s)

    return sum_hist

# Returns a histogram, sampling the single minbias distribution a random number of times,
# given by a Poisson distribution of mean mu.

def SampleMu(q, hist, mu):
    sum_hist = TH1F("sample_mu_" + str(mu), "sample_mu_"+str(mu), 1500, 0, math.ceil( (1-q)*mu) * 1500) #variable histogram sizes             
    sum_hist.Sumw2()
    random.seed(1)
    
    for i in range(0, 100000):
	    n = np.random.poisson(mu)
            s = Sum(q, hist, n)
            sum_hist.Fill(s)

    return sum_hist


# Samples Distribution N times, returns sum.
def Sum(q, hist, N):
	s = 0;
        for n in range(0, N):
		if random.random() > q: # Fraction of Time Energy is non-0.
			s = s + hist.GetRandom()
	return s


#Analytically calcualtes the standard deviation for single minbias
# <x^2> - <x>^2
def CalcStdDev1(q, hist):
	mu = CalcMean1(q, hist)
	variance = (1-q) * ( pow(hist.GetStdDev(),2) + pow(hist.GetMean(), 2) ) - pow(mu, 2) 
	return pow(variance, 0.5)

#Analytically calcualtes mean for single minbias
def CalcMean1(q, hist):
	mu = (1 - q) * hist.GetMean()
	return mu

    

if __name__ == '__main__':

    parser = ap.ArgumentParser(description = 'q: fraction of time 0-energy is deposited in the cell \n' 
                               ' histname: name of histogram to sample from \n',
                               formatter_class=ap.RawTextHelpFormatter)

    parser.add_argument( '--q' , type=float)
    parser.add_argument( '--histname' , type=str)

    args = parser.parse_args()

    PrintOut(args.q, args.histname)
