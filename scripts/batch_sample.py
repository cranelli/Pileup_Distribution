#!/usr/bin/env python
from SamplePU import *

CHANNELS = [ (0.967, "Energy_0_eta1.55"),
             (0.946, "Energy_0_eta1.65"),
             (0.930, "Energy_0_eta1.75"),
             (0.912, "Energy_0_eta1.85"),
             (0.895, "Energy_0_eta2.05"),
             (0.878, "Energy_0_eta2.15"),
             (0.855, "Energy_0_eta2.25"),
             (0.831, "Energy_0_eta2.35"),
             (0.804, "Energy_0_eta2.45"),
             (0.778, "Energy_0_eta2.6"),
             (0.508, "Energy_0_eta2.8"),
             (0.328, "Energy_0_eta3"),
             (0.386, "Energy_0_eta3.2"),
]

for channel in CHANNELS:
    PrintOut(channel[0], channel[1])
