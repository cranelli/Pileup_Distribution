#!/usr/bin/env python 

#Python Code for weighting and summing  all
# the Histograms from two root files.
# Example:
# python weightAndAddHistograms pu_pdf_histograms_mb_low.root pu_pdf_histograms_mb_high.root sum_minbias_low_high.root

import sys

from ROOT import TFile
from ROOT import TH1F
from ROOT import TH2F

N_MB_LOW = 40000
N_MB_HIGH = 10000

FRAC_MB_LOW =  0.99652
FRAC_MB_HIGH = 0.0034532

WEIGHT1 = FRAC_MB_LOW / N_MB_LOW
WEIGHT2 = FRAC_MB_HIGH / N_MB_HIGH


#doCalcC=True

def weightAndAddHistograms(file1Loc, file2Loc, outFileLoc):
    print "weight minbias low: ", WEIGHT1
    print "weight minbias high: ", WEIGHT2

    #Iterate over all the Histograms in the first File
    file1 = TFile(file1Loc, 'READ')
    file2 = TFile(file2Loc, 'READ')
    outFile = TFile(outFileLoc, "RECREATE")
    
    loopList(file1, file2)

def loopList(dir1, dir2):
    list = dir1.GetListOfKeys()
    for key in list:
        # Loop Over Sub Directories
        if key.GetClassName() == "TDirectoryFile":
            print "Directory" , key.GetName()
            new_dir1 = dir1.Get(key.GetName())
            new_dir2 = dir2.Get(key.GetName())
            loopList(new_dir1, new_dir2)
            continue
        
        # Skip Trees
        if key.GetClassName() == "TTree" : continue
    
        # Combine and Write Histograms
        histName = key.GetName()
    
        print histName
        hist1 = dir1.Get(histName)
        hist2 = dir2.Get(histName)

        # Make Sure A Second Histogram of the Same Name Existed
        # print hist2.Class()
        # if hist2.Class() == "TObject" : continue
        #print hist1.GetBinContent(2)
        #print hist2.GetBinContent(2)

        weightedSumHist = hist1.Clone(histName)
        weightedSumHist.Reset("CE")

        weightedSumHist.Add(hist1, hist2, WEIGHT1, WEIGHT2)
        #print weightedSumHist.GetBinContent(2)
        weightedSumHist.Write()

           
if __name__=="__main__":
    weightAndAddHistograms(sys.argv[1], sys.argv[2], sys.argv[3])
    
