ROOT_CFLAGS =$(shell root-config --cflags)
ROOT_LIBS = $(shell root-config --glibs)

SRC_DIR := src
#EXE_DIR := .

EXE = pileup_pdf.exe
SRC := $(wildcard $(SRC_DIR)/*.cc)
INC= -I include

all:
	g++ -g $(ROOT_CFLAGS)  $(ROOT_LIBS) $(INC) -o $(EXE) $(SRC)

clean:
	rm $(EXE)


#g++ -g $(ROOT_CFLAGS)  $(ROOT_LIBS) -o pileup_pdf.exe pileup_pdf.cc histogram_store.cc
	#rm pileup_pdf.exe