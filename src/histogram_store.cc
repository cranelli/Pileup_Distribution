#include "histogram_store.h"

#include "TH1.h"
#include "TH1F.h"
#include "TH3F.h"

using namespace std;

void HistogramStore::Fill(string key, float val, double weight, int n_bins, float xmin, float xmax, string xaxis_title, string yaxis_title){
  if(!histograms_.count(key) ){
    histograms_[key] = new TH1F(Form("%s",key.c_str()), Form("%s",key.c_str()), n_bins, xmin, xmax);
    histograms_[key]->GetXaxis()->SetTitle(xaxis_title.c_str());
    histograms_[key]->GetYaxis()->SetTitle(yaxis_title.c_str());
    histograms_[key]->Sumw2();
  }
  histograms_[key]->Fill(val, weight);
}

/*
void HistogramStore::Fill3D_Variable(string key, float val_x, float val_y, float val_z, double weight, int nbins_x, float * xbins, int nbins_y, float * ybins, int nbins_z, float * zbins, string xaxis_title, string yaxis_title, string zaxis_title){
  
  if(!histograms_.count(key) ){
    histograms_[key] = new TH3F(Form("%s",key.c_str()), Form("%s",key.c_str()), nbins_x, xbins, nbins_y, ybins, nbins_z, zbins);
    histograms_[key]->GetXaxis()->SetTitle(xaxis_title.c_str());
    histograms_[key]->GetYaxis()->SetTitle(yaxis_title.c_str());
    histograms_[key]->GetZaxis()->SetTitle(zaxis_title.c_str());
    histograms_[key]->Sumw2();
  }
  TH3F * hist3D = (TH3F *) histograms_[key];
  hist3D->Fill(val_x, val_y, val_z, weight);
  //(TH3F *) histograms_[key]->Fill(val_x, val_y, val_z, weight);
}
*/

void HistogramStore::Write(){
  for( map<string, TH1 *>::iterator it = histograms_.begin(); it!= histograms_.end(); ++it){
    it->second->Write();
  }
}
