/*
 * Code for generating histograms of single minbias energy distributions for an individual cell.
 * Written by Christopher Anelli
 *                                                                                                  
 * Example Exections                                                                                
 * ./pileup_pdf.exe "/hep300/data/cranelli/LAr/mc15_14TeV_s3142/AREUS_INPUT/MinBias/mc15_14TeV.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e1133_s3142_s3144/areus_ntuple.root" "pu_pdf_histograms_minbias_low.root" "gend3pd"
 * ./pileup_pdf.exe "/hep300/data/cranelli/LAr/mc15_14TeV_s3142/AREUS_INPUT/MinBias/mc15_14TeV.119996.Pythia8_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e1133_s3142_s3144/areus_ntuple.root" "pu_pdf_histograms_minbias_high.root" "gend3pd"
 *
 * ./pileup_pdf.exe "/hep300/data/cranelli/LAr/mc15_14TeV_s3142/AREUS_INPUT/MinBias/mc15_14TeV.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e1133_s3142_s3143/AREUS_INPUT.root" "pu_pdf_histograms_minbias_low.root" "gend3pd"
 */

#define pileup_pdf_cxx
#include "pileup_pdf.h"
#include "histogram_store.h"

#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

#include <vector>
#include <iostream>
#include <sstream>
#include <map>
#include <cmath>

const int NUM_HEC_LAYERS=4;
const int NUM_HEC_ETAS=28;
const int NUM_HEC_PHIS=64;


//void ResetCells( vector< float> & hec_cells);
//void CountZeroEnergyCells(vector<float> & hec_cells, vector<int> & zero_count);


using namespace std;

int main(int argc, char* argv[]){
  //Catch if the user does not supply enough command line arguments                                 
  if(argc !=4){
    cout << "useage: " << argv[0] << " <infilename> <outfilename> <treename>" << endl;
    return 0;
  }
  string in_file_name = argv[1];
  string out_file_name = argv[2];
  string tree_name = argv[3];

  TFile * infile = new TFile(in_file_name.c_str(), "READ");
  TTree * tree = (TTree *) infile->Get(tree_name.c_str());

  TFile * outfile = new TFile(out_file_name.c_str(), "RECREATE");

  //Use pileup_pdf class to analyze root ntuple
  pileup_pdf pu_pdf_analyzer(tree);
  pu_pdf_analyzer.Loop();  

  pu_pdf_analyzer.histogram_store.Write();

}



void pileup_pdf::Loop()
{

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;
   //TestMapping();
   
   //Cells

   //Store Cell Energies in a vector
   vector <float> hec_cells(NUM_HEC_LAYERS*NUM_HEC_ETAS*NUM_HEC_PHIS);
   vector <int> zero_count(NUM_HEC_LAYERS*NUM_HEC_ETAS*NUM_HEC_PHIS);
   

     //vector< vector< vector< float > > > hec_cells = InitializeCells();

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      
      histogram_store.Fill("Event_Count", 1, 1.0, 2, 0, 2, "Passed", "N");

      //Loop Over HEC Cells
      ResetCells(hec_cells); //Set values to 0
      for(int hit_index =0; hit_index < hithec_n; hit_index++){
	
	unsigned int hit_ID = hithec_ID->at(hit_index);
	float hit_eta = hithec_eta->at(hit_index);
	float hit_phi = hithec_phi->at(hit_index);
	int cell_index = GetCellIndex(hit_ID, hit_eta, hit_phi);
	
	float hit_energy = hithec_E->at(hit_index);
	
	//cout << "hit energy" << hit_energy << endl;
	//float sum_energy = hec_cells[cell_index];
	///cout << "sum energy" << sum_energy << endl;
	
	//cout << "Layer:" << layer_index << " Eta:" << eta_index << "Phi:" << phi_index << endl;
	//cout << "Cell Index" << cell_index << endl;
	
	//Check for Double Hit
	//if( hec_cells[cell_index] != 0){
	  //cout << "Double Hit" << endl;
	//}
	hec_cells[cell_index] += hit_energy;
      }

      //Use summed energies to fill histograms

      //Count Cells with 0 energy deposited
      CountZeroEnergyCells(hec_cells, zero_count);
      FillEnergyHistograms(hec_cells);
   }
   
   CalcQ(zero_count, nentries);
}

/*
 * Calculates q, faction of time a cell has no energy deposited.  Measured for each channel, 
 * average over phi, negative/positive eta, and total events.
 */
void pileup_pdf::CalcQ(vector<int> & zero_counts, int nentries){
  float eta;
  float avg_q;
  cout << "Layer, Eta, q" << endl;
  for(int layer =0; layer <4; layer++){
    for(eta = 1.55; eta < 2.5; eta+=0.1){
      //average over phi and positive and negative eta
      avg_q = (LoopPhi_Avgq(zero_counts, layer, eta, NUM_HEC_PHIS) + LoopPhi_Avgq(zero_counts, layer, -1 *eta, NUM_HEC_PHIS) )/2.0;
      avg_q/=nentries;
      cout << layer << "," << eta << "," << avg_q << endl;
    }
    
    for(eta = 2.6; eta < 3.3; eta+=0.2){
      avg_q = (LoopPhi_Avgq(zero_counts, layer, eta, NUM_HEC_PHIS/2.0) + LoopPhi_Avgq(zero_counts, layer, -1 *eta, NUM_HEC_PHIS/2.0) )/2.0;
      avg_q/=nentries;
      cout << layer << "," << eta << "," << avg_q << endl;
    }    
  }		 
}

//Calculates the Average q of a given layer/eta, over the full phi range.
float pileup_pdf::LoopPhi_Avgq(vector<int> & zero_counts, int layer, float eta, int num_phi_cells){
  int id = layer*512;
  float avg_q =0;
  for(float phi = -3.14; phi < 3.14; phi+=6.284/num_phi_cells){
    avg_q += zero_counts[GetCellIndex(id, eta, phi)];
  }
  avg_q/=(1.0*num_phi_cells);
  return avg_q;
}


void pileup_pdf::FillEnergyHistograms(vector<float> & hec_cells){
  //int id;
  float eta;
  //float phi,eta;
  //float energy;
  //stringstream key_ss;

  for(int layer =0; layer <4; layer++){
    for(eta = 1.55; eta < 2.5; eta+=0.1){
      LoopPhi_EnergyFill(layer, eta, NUM_HEC_PHIS, hec_cells);
      LoopPhi_EnergyFill(layer, -1*eta, NUM_HEC_PHIS, hec_cells); //positive and negative eta.
    }
    for(eta = 2.6; eta < 3.3; eta+=0.2){
      LoopPhi_EnergyFill(layer, eta, NUM_HEC_PHIS/2.0, hec_cells);
      LoopPhi_EnergyFill(layer, -1*eta, NUM_HEC_PHIS/2.0, hec_cells);
    }
  }
}

/*
 * Looping over phis of a given layer and eta.  Channels are associated with layer and abs(eta). 
 */
void pileup_pdf::LoopPhi_EnergyFill(int layer, float eta, int num_phi_cells, vector<float> & hec_cells){
  stringstream key_ss;
  int id = layer*512;
  key_ss << "Energy_" << layer << "_" << "eta" << abs(eta);
  for(float phi = -3.14; phi < 3.14; phi+=6.284/num_phi_cells){
    float energy = hec_cells[GetCellIndex(id, eta, phi)];
    //key, val, weight, nbins, xmin, xmax, xtitle, ytitle
    if( energy > 0) {
      histogram_store.Fill(key_ss.str(), energy, 1.0, 300, 1.0, 1500, "Energy (MeV)", "N");
      histogram_store.Fill(key_ss.str()+"_40MeV", energy, 1.0, 100, 1.1, 40, "Energy (MeV)", "N");
      histogram_store.Fill(key_ss.str()+"_5MeV", energy, 1.0, 50, 1.1, 5, "Energy (MeV)", "N");
      //histogram_store.Fill(key_ss.str()+"_10KeV", energy, 1.0, 100, 0, 0.01, "Energy (MeV)", "N");
    }
  }
}



void pileup_pdf::TestMapping(){
  int layer,id;
  float phi, eta;
  //eta = -3.3;
  // while(eta < 3.3){
  for(layer =0; layer <4; layer++){
    for(eta = -3.25; eta < 3.3; eta+=0.1){
      if(abs(eta) < 1.5) continue;
      cout << "Layer-Eta " << layer <<"-"<< eta << ": " << endl;
      for(phi = -3.14; phi < 3.14; phi+=6.284/64.0){
	//cout << phi << endl;
	//cout << GetPhiIndex(phi, eta) << endl;
	id = layer*512;
	cout << GetCellIndex(id,eta,phi) << ",";
      }
    cout << endl;
    }
  }
}



//Cell size switches between 0.1x0.1 to 0.2x0.2 above eta=2.5
int pileup_pdf::GetCellIndex(int id, float eta, float phi){
  int layer_index = id/512;
  int eta_index = GetEtaIndex(eta);
  int phi_index = GetPhiIndex(phi , eta);
  
  int cell_index = layer_index*(NUM_HEC_ETAS*NUM_HEC_PHIS)+eta_index*NUM_HEC_PHIS+phi_index; 
  //cout << cell_index << endl;
  return cell_index;
}

int pileup_pdf::GetPhiIndex(float phi, float eta){
  int phi_index;
  if(abs(eta) >= 2.5){
    phi_index = 16 + 16*phi/3.1416;
  } else {
    phi_index = 32 + 32*phi/3.1416;
  }   

  return phi_index;
}

int pileup_pdf::GetEtaIndex(float eta){
  int eta_index;
  if(eta < -2.5){
    eta_index =  (eta + 3.3)/0.2;
    //cout << eta_index << endl;
  }
  if(eta > -2.5 && eta < -1.5){
    eta_index = 4 + (eta + 2.5)/0.1;
  }
  if(eta > 1.5 && eta < 2.5){
    eta_index =14 + (eta-1.5)/0.1;
  }
  if(eta > 2.5){
    eta_index = 24 + (eta -2.5)/0.2;
  }   
  return eta_index;
}

void pileup_pdf::CountZeroEnergyCells(vector<float> & hec_cells, vector<int> & zero_count){
  for(int i =0; i< hec_cells.size(); i++){
    if(hec_cells[i] == 0){
      zero_count[i]++;
    }
  }
}

/*
vector< vector< vector< float > > > InitializeCells(){
  vector< vector< vector< float > > > hec_cells(NUM_HEC_LAYERS, vector< vector <float> >(NUM_HEC_ETAS, vector<float>()));
  for(int i = 0; i < NUM_HEC_LAYERS; i++){
    for(int j=0; j < NUM_HEC_ETAS; j++){
      //Separate between 0.1x0.1 cells and 0.2x0.2
      vector<float> phi_cells;
      if(j < 4 || j >= NUM_HEC_ETAS-4){
	phi_cells = vector<float>(32);
	//vector<double> phi_cells(12);
      } else {
	phi_cells = vector<float>(64);
      }
      hec_cells[i][j] = phi_cells;

    }
  }
  return hec_cells;
}
*/


//Resets all values stored the vector to 0, keeps the size of the vector the same.
void pileup_pdf::ResetCells( vector<float>  & hec_cells){
  fill(hec_cells.begin(), hec_cells.end(), 0);
}
