# Pileup Histograms

This analysis code uses simulated minbias events to produce histograms showing the probability distrubtion of energies in a given LAr cell, as well as **q**, the fraction of the time no energy is deposited.   

These distributions can be compared to those analytically predicted in [HEC note 110](https://kurchan.web.cern.ch/Notes/rodsim.pdf).  

Sampling from these distributions allows one to calculate the in-time pileup noise for different scenarios.  In particular for <μ>=200, one can demonstrate that the formula for the noise should indeed be sqrt(200*(μ_1^2+σ_1^2)) and not sqrt(200)*σ_1.  Where μ_1 and σ_1 are the average energy and standard deviation for a single minbias event.

# Inputs
Ntuples built from single minbias HIT files are taken as the inputs for this analysis code. Ntuples currently stored on /hep300/data of Symmetry, ie:
>/hep300/data/cranelli/LAr/mc15_14TeV_s3142/AREUS_INPUT/MinBias/mc15_14TeV.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e1133_s3142_s314/areus_ntuple.root
>/hep300/data/cranelli/LAr/mc15_14TeV_s3142/AREUS_INPUT/MinBias/mc15_14TeV.119996.Pythia8_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e1133_s3142_s3143/areus_ntuple.root

Ntuples were produced using [AreusCaloTools](https://gitlab.cern.ch/AREUS/AreusCaloTools), and instructions for doing so can be found on the AREUS repository and at the end of this README.

# Initial Installation
The analysis code is written in C++ to speedup running ove Ntuples.  A custom makefile for compiling is included in the repository. (Source code and corresponding header files are located in src/ and include/).  The code makes use of ROOT libraries, so a version of ROOT must be setup prior to compiling or running.  If successfully, an executable called **pileup_pdf.exe** will be created.

> git clone https://gitlab.com/cranelli/Pileup_Distribution.git  
> cd Pileup_Distribution  
> setupATLAS  
> lsetup root    
> make  

# Next time
> lsetup root  

If you want a fresh compile and to delete the objects and executables.
> make clean

# Run pileup_pdf.exe
The executable expects 3 parameters to be passed: the path to the input minbias ntuple, the path to the output root file, which will contain the energy histograms, and the name of the ntuple tree.  As it runs, **pileup_pdf.exe** also prints out the q-value for each of the LAr cells, so it is often useful to pipe these into a .csv file.

> ./pileup_pdf.exe "/hep300/data/cranelli/LAr/mc15_14TeV_s3142/AREUS_INPUT/MinBias/mc15_14TeV.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e1133_s3142_s3143/AREUS_INPUT.root" "pu_pdf_histograms_minbias_low.root" "gend3pd" > q_low.csv

Minbias generation is split between high and low pT samples.  It is recomended to run **pileup_pdf.exe** over both the high and low minbias ntuples.  Combining the high and low histograms is described in the next section.

# Combine High and Low Histograms
The high and low minbias histograms can be summed together using the **weightAndAddHistograms.py** script. 
Modify the script so that  **N_MB_LOW** and **N_MB_HIGH** equal the number of events in the low and high minbias root files.
The script also defines the fraction of events that are high pT and low pT and should be modified depending on the pileup scenario.
 
> ./scripts/weightAndAddHistograms.py pu_pdf_histograms_mb_low.root pu_pdf_histograms_mb_high.root sum_minbias_low_high.root

# Sample Histograms
One can randomly sample from the minbias energy distribution histograms multiple times to see what the energy distribution will look like for different pileup scenarios.  Looking at 100,000 cases, **SamplePU.py** compares the energies distributions and standard deviation for sampling N=1, N=200, or N=Pois(<μ>=200) times.  To run the script one passes as parameters the histogram from which you want to randomly sample and the histogram's corresponding q-value.  Note, the script must be called from a working directory containing <sum_minbias_low_high.root>.  

> ./scripts/SamplePU.py --q 0.967 --histname Energy_0_eta1.55

To run over all LAr cells at once, **batch_sample.py** feeds a list of histogram_names and q-values to SamplePU.  The output is printed in a LaTeX format so it can be easily converted into a table.
> ./scripts/batch_sample.py > table.tex


# Additional Scripts
To print out the contents of the histogrmas, **hist2txt.py** converts histograms into .dat files.  Given the large number of files, these are often useful to tar.
> ./scripts/hist2txt.py sum_minbias_low_high.root  
> tar -czf  PileupDataFiles.tar.gz *.dat  
> rm -rf *.dat  

# Notes
Interface to decode and generate offline identifiers  
for the HEC detector subsystem, from LArHEC_ID class.  

-------           -----              -------
element           range              meaning
-------           -----              -------

pos/neg          -2 or 2          -2 = negative HEC (C side), 2 = positive HEC  ( A side)  
sampling         [0,3]            [0,1] = first wheel, [2,3] = second wheel  
region           [0,1]            0 : outer part, 1 : inner part  
eta              [0,9]            Outer part region 0 , samplings 0 and 1 , 1.5< eta <2.5 , deta=0.1  
                 [1,9]            Outer part region 0 , samplings 2       , 1.6< eta <2.5 , deta=0.1  
                 [2,9]            Outer part region 0 , samplings 3       , 1.7< eta <2.5 , deta=0.1  
                 [0,3]            Inner part region 1 , samplings 0 and 3 , 2.5< eta <3.3 , deta=0.2  
                 [0,2]            Inner part region 1 , samplings 1 and 2 , 2.5< eta <3.1 , deta=0.2  
phi              [0,63]           Outer part, dphi=0.1  
                 [0,31]           Inner part, dphi=0.2  
-------           -----              -------

# Input Preparation

The areus input ntuples are prepared using [AreusCaloTools](https://gitlab.cern.ch/AREUS/AreusCaloTools):
> git clone ssh://git@gitlab.cern.ch:7999/AREUS/AreusCaloTools.git
  
Running locally over HIT files (s3143):
> athena AreusCaloTools/share/MakeAreusInputFile.py -c 'infile="/hep300/data/cranelli/LAr/mc15_14TeV_s3142/HITS/Minbias/mc15_14TeV.119995.Pythia8_A2MSTW2008LO_minbias_ineleastic_low.merge.HITS.e1133_s3142_s3143/*.root*"; outfile="areus_ntuple.root"'  

> athena AreusCaloTools/share/MakeAreusInputFile.py -c 'infile = "/hep300/data/cranelli/LAr/mc15_14TeV_s3142/HITS/Minbias/mc15_14TeV.119996.Pythia8_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e1133_s3142_s3143/*.root*"; outfile="areus_ntuple.root"'  

Running on the GRID over the full HIT samples (s3143):
> pathena AreusCaloTools/share/MakeAreusInputFile.py -c 'infile="HITS.minbias_low.s3142_s3143.test.root"; outfile="areus_ntuple.root"' --nFilesPerJob=10 --nJobs=200 --inDS=mc15_14TeV.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e1133_s3142_s3143 --outDS=user.cranelli.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e1133_s3142_s3143

> pathena AreusCaloTools/share/MakeAreusInputFile.py -c 'infile="HITS.minbias_high.s3142_s3143.test.root"; outfile="areus_ntuple.root"' --nFilesPerJob=10 --nJobs=100 --inDS=mc15_14TeV.119996.Pythia8_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e1133_s3142_s3143 --outDS=user.cranelli.119996.Pythia8_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e1133_s3142_s3143
